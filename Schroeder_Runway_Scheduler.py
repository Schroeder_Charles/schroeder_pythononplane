"""Below are global variables:"""

schedule = []
rawData = []
dynamicQueue = []

print "Please ensure your document is properly formatted. Example: Delta 23, 0, 2, 4"

"""This function handles opening the file, and then sending the constructed flight objects to the schedule array"""

def fileHandler():
	fileName = raw_input('Enter File Name: ')

	with open(fileName, 'r') as f:
		data = f.readlines()

	for line in data:
		flightinfo = line.split(",")
		if len(flightinfo) < 4:
			print "Invalid Data: " + str(flightinfo)
		else:	
			schedule.append(flightObjectConstructor(flightinfo))
			rawData.append(flightObjectConstructor(flightinfo))
			print flightinfo	

"""This class constructs the flight object"""

class flightObjectConstructor:
	def __init__(self, flightinfo):
		self.flightName = flightinfo[0]
		self.subTime = int(flightinfo[1])
		self.requestedTime = int(flightinfo[2])
		self.duration = int(flightinfo[3])

"""This function implements a simple insertion sort, and prioritizing earliest submissions"""

def queueHandler(schedule):

	currentTime = 0

	for i in range(1,len(schedule)):
		sortingIndex = i

		while sortingIndex > 0 and schedule[sortingIndex].requestedTime < schedule[sortingIndex -1].requestedTime:
			schedule[sortingIndex], schedule[sortingIndex-1] = schedule[sortingIndex-1] , schedule[sortingIndex]
			sortingIndex = sortingIndex-1

		if schedule[sortingIndex].requestedTime == schedule[sortingIndex-1].requestedTime:
			if schedule[sortingIndex].subTime < schedule[sortingIndex-1].subTime:
				schedule[sortingIndex], schedule[sortingIndex-1] = schedule[sortingIndex-1] , schedule[sortingIndex]
				sortingIndex = sortingIndex-1

	for x in range(len(schedule)):
		print schedule[x].flightName + " (" + str(currentTime) + "-" + str((currentTime + int(schedule[x].duration))) + ")"
		
		currentTime = currentTime + int(schedule[x].duration)
	return schedule	

"""This function handles user input if the user wishes to create their own list of flight info"""

def manualInputHandler():
	moreData = True

	while moreData == True:
		flightinfo = []

		manualFlightData = raw_input('Enter the flight name: ')
		flightinfo.append(manualFlightData)

		manualFlightData = raw_input('Enter submission time: ')
		flightinfo.append(manualFlightData)

		manualFlightData = raw_input('Enter requested time: ')
		flightinfo.append(manualFlightData)

		manualFlightData = raw_input('Enter duration: ')
		flightinfo.append(manualFlightData)

		schedule.append(flightObjectConstructor(flightinfo))

		moreDataYN = raw_input('Enter more data? (y or n): ')
		if moreDataYN == 'n':
			moreData = False

	queueHandler(schedule)

def lookUpByTimeHandler(rawData):
	
	for i in range(1,len(schedule)):
		sortingIndex = i

		while sortingIndex > 0 and schedule[sortingIndex].subTime < schedule[sortingIndex -1].subTime:
			schedule[sortingIndex], schedule[sortingIndex-1] = schedule[sortingIndex-1] , schedule[sortingIndex]
			sortingIndex = sortingIndex-1

		if schedule[sortingIndex].subTime == schedule[sortingIndex-1].subTime:
			if schedule[sortingIndex].requestedTime < schedule[sortingIndex-1].requestedTime:
				schedule[sortingIndex], schedule[sortingIndex-1] = schedule[sortingIndex-1] , schedule[sortingIndex]
				sortingIndex = sortingIndex-1	

inputMethod = raw_input('To read from a file type file. To enter input manually type manual: ')

if len(inputMethod) == 4:
	fileHandler()

elif len(inputMethod) == 6:
	manualInputHandler()

queueHandler(schedule)
lookUpByTimeHandler(rawData)

moreSearches = True

while moreSearches == True:
	userSearch = raw_input('Would you like to check the queue at a certain time? (y or n) ')
	if userSearch == 'y':
		searchTime = raw_input('Enter a time to see the queue status (hour only) ')

		for q in range(0, int(searchTime)):
			dynamicQueue.append(rawData[q])

		queueHandler(dynamicQueue)
		del dynamicQueue[:]

	else:
		moreSearches = False 
		print "Exiting program..."	
